<?php

require  '../vendor/autoload.php';

Safrapay\ApiSDK\Configuration::initialize();
Safrapay\ApiSDK\Configuration::setUrl(Safrapay\ApiSDK\Configuration::DEV_URL);
Safrapay\ApiSDK\Configuration::setCnpj("83032272000109");
Safrapay\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
Safrapay\ApiSDK\Configuration::setlog(true);
Safrapay\ApiSDK\Configuration::login();

$gateway = new Safrapay\ApiSDK\Gateway;
$boleto = new Safrapay\ApiSDK\Domains\Boleto;

$boleto->setDeadline("2");

// Customer
$boleto->customer->setId("00002");
$boleto->customer->setName("fulano");
$boleto->customer->setEmail("fulano@safrapay.co");
$boleto->customer->setDocumentType(Safrapay\ApiSDK\Enum\DocumentType::CPF);
$boleto->customer->setDocument("14533859755");

// Customer->address
$boleto->customer->address->setStreet("Avenida Salvador");
$boleto->customer->address->setNumber("5401");
$boleto->customer->address->setNeighborhood("Recreio dos bandeirantes");
$boleto->customer->address->setCity("Rio de janeiro");
$boleto->customer->address->setState("RJ");
$boleto->customer->address->setCountry("BR");
$boleto->customer->address->setZipcode("2279714");
$boleto->customer->address->setComplement("");

// Customer->phone
$boleto->customer->phone->setCountryCode("55");
$boleto->customer->phone->setAreaCode("21");
$boleto->customer->phone->setNumber("98491715");
$boleto->customer->phone->setType(Safrapay\ApiSDK\Enum\PhoneType::MOBILE);

// Transactions
$boleto->transactions->setAmount(30000);
$boleto->transactions->setInstructions("Crédito de teste");

// Transactions->fine (opcional)
$boleto->transactions->fine->setStartDate("2");
$boleto->transactions->fine->setAmount(300);
$boleto->transactions->fine->setInterest(10);

// Transactions->discount (opcional)
$boleto->transactions->discount->setType(Safrapay\ApiSDK\Enum\DiscountType::FIXED);
$boleto->transactions->discount->setAmount(200);
$boleto->transactions->discount->setDeadline("1");

$resBoleto = $gateway->charge($boleto);

echo "\n\nResposta:\n";
print_r(json_encode($resBoleto));

if (isset($resBoleto["status"])) {
    if ($resBoleto["status"] == Safrapay\ApiSDK\Enum\ChargeStatus::PRE_AUTHORIZED) 
        echo "\n\nPRÉ AUTORIZADO!\n";
} else {
    if ($resBoleto != NULL)
        echo "httStatus: ".$resBoleto["httpStatus"]
            ."\n httpMsg: ".$resBoleto["httpMsg"]
            ."\n";
}

$res = $gateway->billingInformation($resBoleto["charge"]->id);

echo "\n\nResposta:\n";
print_r(json_encode($res));

if (isset($res["status"])) {
    echo "\n\n".$res["status"]."\n";
} else {
    echo "\nhttStatus: ".$res["httpStatus"]
        ."\nhttpMsg: ".$res["httpMsg"]
        ."\n";
}