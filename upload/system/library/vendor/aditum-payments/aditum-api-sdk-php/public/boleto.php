<?php

require  '../vendor/autoload.php';

Safrapay\ApiSDK\Configuration::initialize();
Safrapay\ApiSDK\Configuration::setUrl(Safrapay\ApiSDK\Configuration::DEV_URL);
Safrapay\ApiSDK\Configuration::setCnpj("83032272000109");
Safrapay\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
Safrapay\ApiSDK\Configuration::setlog(true);
Safrapay\ApiSDK\Configuration::login();

$gateway = new Safrapay\ApiSDK\Gateway;
$boleto = new Safrapay\ApiSDK\Domains\Boleto;

$boleto->setMerchantChargeId("");
$boleto->setSessionId("");

$boleto->setDeadline("2");

// Products = Nome, SKU, Valor, Quantidade
$boleto->products->add("Jackson 2", "32424242", 1001, 1);
$boleto->products->add("Jackson 3", "32424242", 1002, 2);
$boleto->products->add("Jackson 4", "32424242", 1003, 3);

// Customer
$boleto->customer->setId("00002");
$boleto->customer->setName("fulano");
$boleto->customer->setEmail("fulano@safrapay.co");
$boleto->customer->setDocumentType(Safrapay\ApiSDK\Enum\DocumentType::CPF);
$boleto->customer->setDocument("14533859755");

// Customer->address
$boleto->customer->address->setStreet("Avenida Salvador");
$boleto->customer->address->setNumber("5401");
$boleto->customer->address->setNeighborhood("Recreio dos bandeirantes");
$boleto->customer->address->setCity("Rio de janeiro");
$boleto->customer->address->setState("RJ");
$boleto->customer->address->setCountry("BR");
$boleto->customer->address->setZipcode("2279714");
$boleto->customer->address->setComplement("");

// Customer->phone
$boleto->customer->phone->setCountryCode("55");
$boleto->customer->phone->setAreaCode("21");
$boleto->customer->phone->setNumber("98491715");
$boleto->customer->phone->setType(Safrapay\ApiSDK\Enum\PhoneType::MOBILE);

// Transactions
$boleto->transactions->setAmount(30000);
$boleto->transactions->setInstructions("Crédito de teste");

// Transactions->fine (opcional)
$boleto->transactions->fine->setStartDate("2");
$boleto->transactions->fine->setAmount(300);
$boleto->transactions->fine->setInterest(10);

// Transactions->discount (opcional)
// $boleto->transactions->discount->setType(Safrapay\ApiSDK\Enum\DiscountType::FIXED);
// $boleto->transactions->discount->setAmount(200);
// $boleto->transactions->discount->setDeadline("1");

$res = $gateway->charge($boleto);

echo "\n\nResposta:\n";
print_r(json_encode($res));

if (isset($res["status"])) {
    if ($res["status"] == Safrapay\ApiSDK\Enum\ChargeStatus::PRE_AUTHORIZED) 
        echo "\n\nBOLETO!\n";
} else {
    if ($res != NULL)
        echo "\nhttStatus: ".$res["httpStatus"]
            ."\nhttpMsg: ".$res["httpMsg"]
            ."\n";
}