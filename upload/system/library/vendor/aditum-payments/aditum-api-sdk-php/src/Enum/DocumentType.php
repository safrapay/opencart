<?php

namespace Safrapay\ApiSDK\Enum;

abstract class DocumentType {
    public const CPF  = 1;
    public const CNPJ = 2;

}