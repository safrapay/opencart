# Authentication
A autenticação é a forma que deve ser utilizada para gerar um Token de acesso, para que você consiga utilizar os endpoints.

```php
Safrapay\ApiSDK\Configuration::initialize();

Safrapay\ApiSDK\Configuration::setUrl(Safrapay\ApiSDK\Configuration::DEV_URL); // Caso não defina a url, será usada de produção
Safrapay\ApiSDK\Configuration::setCnpj("83032272000109");
Safrapay\ApiSDK\Configuration::setMerchantToken("mk_P1kT7Rngif1Xuylw0z96k3");
Safrapay\ApiSDK\Configuration::setlog(true);

$res = Safrapay\ApiSDK\Configuration::login();

if (isset($res["token"])) {
	echo  $res["token"]."\n";
	
} else {
	echo  "httStatus: ".$res["httpStatus"]
		."\n httpMsg: ".$res["httpMsg"]
		."\n";
}
```
