# ChargeStatus

### Status da transação
```php
Safrapay\ApiSDK\Enum\ChargeStatus::AUTHORIZED;        // Todas as transações da cobrança foram aprovadas.
Safrapay\ApiSDK\Enum\ChargeStatus::PRE_AUTHORIZED;    // Todas as transações da cobrança foram pré-autorizadas.
Safrapay\ApiSDK\Enum\ChargeStatus::CANCELED;          // Todas as transações da cobrança foram canceladas.
Safrapay\ApiSDK\Enum\ChargeStatus::PARTIAL;           // As transações da cobrança diferem em status. Verificar o status de cada transação individualmente.
Safrapay\ApiSDK\Enum\ChargeStatus::_AUTHORIZED;       // Todas as transações da cobrança foram negadas.
Safrapay\ApiSDK\Enum\ChargeStatus::NOT_PENDING_CANCEL;// Todas as transações da cobrança estão com cancelamento pendente.
```

#

# PaymentType

###  Tipos de pagamentos
```php
Safrapay\ApiSDK\Enum\PaymentType::UNDEFINED;  // Indefinido.
Safrapay\ApiSDK\Enum\PaymentType::DEBIT;      // Débito.
Safrapay\ApiSDK\Enum\PaymentType::CREDIT;     // Crédito.
Safrapay\ApiSDK\Enum\PaymentType::VOUCHER;    // Voucher.
Safrapay\ApiSDK\Enum\PaymentType::BOLETO;     // Boleto.
Safrapay\ApiSDK\Enum\PaymentType::TED ;       // Transferência Eletrônica de Fundos.
Safrapay\ApiSDK\Enum\PaymentType::DOC;        // Documento de Ordem de Crédito.
Safrapay\ApiSDK\Enum\PaymentType::SAFETY_PAY; // SafetyPay.
```
#

# CardBrand

### Nomes das bandeiras
```php
Safrapay\ApiSDK\Enum\CardBrand::VISA;
Safrapay\ApiSDK\Enum\CardBrand::MASTER_CARD;
Safrapay\ApiSDK\Enum\CardBrand::AMEX;
Safrapay\ApiSDK\Enum\CardBrand::ELO;
Safrapay\ApiSDK\Enum\CardBrand::AURA;
Safrapay\ApiSDK\Enum\CardBrand::JCB;
Safrapay\ApiSDK\Enum\CardBrand::DINERS;
Safrapay\ApiSDK\Enum\CardBrand::DISCOVER;
Safrapay\ApiSDK\Enum\CardBrand::HIPERCARD;
Safrapay\ApiSDK\Enum\CardBrand::ENROUTE;
Safrapay\ApiSDK\Enum\CardBrand::TICKET;
Safrapay\ApiSDK\Enum\CardBrand::SODEXO;
Safrapay\ApiSDK\Enum\CardBrand::VR;
Safrapay\ApiSDK\Enum\CardBrand::ALELO;
Safrapay\ApiSDK\Enum\CardBrand::SETRA;
Safrapay\ApiSDK\Enum\CardBrand::VERO;
Safrapay\ApiSDK\Enum\CardBrand::SOROCRED;
Safrapay\ApiSDK\Enum\CardBrand::GREEN_CARD;
Safrapay\ApiSDK\Enum\CardBrand::CABAL;
Safrapay\ApiSDK\Enum\CardBrand::BANESCARD;
Safrapay\ApiSDK\Enum\CardBrand::VERDE_CARD;
Safrapay\ApiSDK\Enum\CardBrand::VALE_CARD;
Safrapay\ApiSDK\Enum\CardBrand::UNION_PAY;
Safrapay\ApiSDK\Enum\CardBrand::UP;
Safrapay\ApiSDK\Enum\CardBrand::TRICARD;
Safrapay\ApiSDK\Enum\CardBrand::BIGCARD;
Safrapay\ApiSDK\Enum\CardBrand::BEN;
Safrapay\ApiSDK\Enum\CardBrand::REDE_COMPRAS;
```

#

# AcquirerCode

### Adquirentes
```php
Safrapay\ApiSDK\Enum\AcquirerCode::CIELO;
Safrapay\ApiSDK\Enum\AcquirerCode::REDE;
Safrapay\ApiSDK\Enum\AcquirerCode::STONE;
Safrapay\ApiSDK\Enum\AcquirerCode::VBI;
Safrapay\ApiSDK\Enum\AcquirerCode::GRANITO;
Safrapay\ApiSDK\Enum\AcquirerCode::INFINITE_PAY;
Safrapay\ApiSDK\Enum\AcquirerCode::SAFRA_PAY;
Safrapay\ApiSDK\Enum\AcquirerCode::ADITUM_ECOM;
Safrapay\ApiSDK\Enum\AcquirerCode::PAGSEGURO;
Safrapay\ApiSDK\Enum\AcquirerCode::ADITUM_TEF;
Safrapay\ApiSDK\Enum\AcquirerCode::SAFRAPAYTEF;
Safrapay\ApiSDK\Enum\AcquirerCode::VR_BENEFITS;
Safrapay\ApiSDK\Enum\AcquirerCode::SIMULADOR;
```
#

# PhoneType

### Tipos de telefones
```php
Safrapay\ApiSDK\Enum\PhoneType::RESIDENCIAL; // Telefone Residencial
Safrapay\ApiSDK\Enum\PhoneType::COMERCIAL;   // Telefone Comercial
Safrapay\ApiSDK\Enum\PhoneType::VOICEMAIL;   // Correio de Voz
Safrapay\ApiSDK\Enum\PhoneType::TEMPORARY;   // Telefone Temporário
Safrapay\ApiSDK\Enum\PhoneType::MOBILE;      // Celular
```
# DocumentType

### Tipos de documentos
```php
Safrapay\ApiSDK\Enum\DocumentType::CPF;
Safrapay\ApiSDK\Enum\DocumentType::CNPJ;
```

# DiscountType

### Tipos de documentos
```php
Safrapay\ApiSDK\Enum\DiscountType::UNDEFINED; // Indefinido
Safrapay\ApiSDK\Enum\DiscountType::PERCENTUAL;// Aplicará desconto percentual com base em uma data de pagamento.
Safrapay\ApiSDK\Enum\DiscountType::FIXED;     // Aplicará um valor fixo em centavos com base na data de pagamento.
Safrapay\ApiSDK\Enum\DiscountType::DAILY;     // Aplicará um valor diário em centavos com base na data de pagamento.
```