<?php
// Heading
$_['heading_title']      = 'Safrapay - Cartão de Crédito';

// Text
$_['text_extension']     = 'Extensions';
$_['text_success']       = 'Success: You have modified Safrapay - Cartão de Crédito payment module!';
$_['text_edit']          = 'Edit Safrapay - Cartão de Crédito';

// Entry
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Help
$_['help_total']         = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment Safrapay - Cartão de Crédito!';

$_['safrapay_cc_modo'] = 'Ambiente do Gateway';
$_['safrapay_cc_titulo_gateway'] = 'Título do Gateway';
$_['safrapay_cc_descricao_gateway'] = 'Descrição do Gateway';
$_['safrapay_cc_instrucoes'] = 'Instruções Após o Pedido';
$_['safrapay_cc_parcela_minima'] = 'Valor mínimo da parcela';
$_['safrapay_cc_maximo_parcelas'] = 'Número máximo de parcelas';
$_['safrapay_cc_cnpj'] = 'CNPJ';
$_['safrapay_cc_merchant_token'] = 'Merchant Token';
$_['safrapay_cc_campo_documento'] = 'Campo Documento (CPF/CNPJ)';
$_['safrapay_cc_campo_numero'] = 'Campo Número da Rua';
$_['safrapay_cc_campo_complemento'] = 'Campo Complemento';
$_['safrapay_cc_tipo_antifraude'] = 'Tipo de Antifraude';
$_['safrapay_cc_token_antifraude'] = 'Token';
$_['safrapay_cc_debug'] = 'Habilitar debug';