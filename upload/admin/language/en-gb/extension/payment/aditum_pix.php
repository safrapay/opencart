<?php
// Heading
$_['heading_title']      = 'Safrapay - Pix';

// Text
$_['text_extension']     = 'Extensions';
$_['text_success']       = 'Success: You have modified Safrapay - Pix payment module!';
$_['text_edit']          = 'Edit Safrapay - Pix';

// Entry
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Help
$_['help_total']         = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment Safrapay - Pix!';

$_['safrapay_pix_modo'] = 'Ambiente do Gateway';
$_['safrapay_pix_titulo_gateway'] = 'Título do Gateway';
$_['safrapay_pix_descricao_gateway'] = 'Descrição do Gateway';
$_['safrapay_pix_instrucoes'] = 'Instruções Após o Pedido';
$_['safrapay_pix_cnpj'] = 'CNPJ';
$_['safrapay_pix_merchant_token'] = 'Merchant Token';
$_['safrapay_pix_campo_documento'] = 'Campo Documento (CPF/CNPJ)';
$_['safrapay_pix_campo_numero'] = 'Campo Número da Rua';
$_['safrapay_pix_campo_complemento'] = 'Campo Complemento';
$_['safrapay_pix_tipo_antifraude'] = 'Tipo de Antifraude';
$_['safrapay_pix_token_antifraude'] = 'Token';
$_['safrapay_pix_debug'] = 'Habilitar debug';