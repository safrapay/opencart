<?php
// Heading
$_['heading_title']      = 'Safrapay - Boleto';

// Text
$_['text_extension']     = 'Extensions';
$_['text_success']       = 'Success: You have modified Safrapay - Boleto payment module!';
$_['text_edit']          = 'Edit Safrapay - Boleto';

// Entry
$_['entry_total']        = 'Total';
$_['entry_order_status'] = 'Order Status';
$_['entry_geo_zone']     = 'Geo Zone';
$_['entry_status']       = 'Status';
$_['entry_sort_order']   = 'Sort Order';

// Help
$_['help_total']         = 'The checkout total the order must reach before this payment method becomes active.';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment Safrapay - Boleto!';

$_['safrapay_billet_modo'] = 'Ambiente do Gateway';
$_['safrapay_billet_titulo_gateway'] = 'Título do Gateway';
$_['safrapay_billet_descricao_gateway'] = 'Descrição do Gateway';
$_['safrapay_billet_instrucoes'] = 'Instruções Após o Pedido';
$_['safrapay_billet_parcela_minima'] = 'Valor mínimo da parcela';
$_['safrapay_billet_maximo_parcelas'] = 'Número máximo de parcelas';
$_['safrapay_billet_cnpj'] = 'CNPJ';
$_['safrapay_billet_merchant_token'] = 'Merchant Token';
$_['safrapay_billet_campo_documento'] = 'Campo Documento (CPF/CNPJ)';
$_['safrapay_billet_campo_numero'] = 'Campo Número da Rua';
$_['safrapay_billet_campo_complemento'] = 'Campo Complemento';
$_['safrapay_billet_tipo_antifraude'] = 'Tipo de Antifraude';
$_['safrapay_billet_token_antifraude'] = 'Token';
$_['safrapay_billet_expiracao'] = 'Tempo de expiração do boleto (Dias):';
$_['safrapay_billet_dias_multa'] = 'Dias para multa:';
$_['safrapay_billet_valor_multa'] = 'Valor fixo da multa:';
$_['safrapay_billet_percentual_multa'] = 'Valor percentual da multa:';
$_['safrapay_billet_debug'] = 'Habilitar debug';